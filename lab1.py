from math import *

__author__ = 'hakami'

INT_MULTIPLIER = 10 ** 7

class Solution:
    a = 0.4
    b = 4.0

    a_int = int(a * INT_MULTIPLIER)
    b_int = int(b * INT_MULTIPLIER)

    def __init__(self, n, m, range_type):
        self.N = n
        self.M = m
        self.__cashed_values = {}
        self.point_ranges = range_type

    def range_regular(N):
        h_int = int((Solution.b - Solution.a) / N * INT_MULTIPLIER)
        return range(Solution.a_int, Solution.b_int + h_int, h_int)

    def range_cheb(N):
        for i in range(N, -1, -1):
            xi = (Solution.b - Solution.a) / 2
            xi *= cos((2 * i + 1) * pi / (2 * (N + 1)))
            xi += (Solution.b + Solution.a) / 2
            yield xi * INT_MULTIPLIER

    def calc(self, func, x):
        x_int = int(x * INT_MULTIPLIER)
        if func in self.__cashed_values and x_int in self.__cashed_values[func]:
            return self.__cashed_values[func][x_int]
        else:
            answer = func(self, x)
            if func not in self.__cashed_values:
                self.__cashed_values[func] = {}
            self.__cashed_values[func][x_int] = answer
            return answer

    def ci(self, x):
        n = 1.0
        elem = -1 * x ** 2 / 2.0
        ans = 0.0
        while abs(elem / (2 * n)) > 10.0 ** -6:
            ans += elem / (2 * n)
            n += 1.0
            elem *= -1 * x ** 2
            elem /= 2 * n * (2 * n - 1)

        return ans

    def ln(self, x):
        ans = 0.0

        for x_i_int in self.point_ranges(self.N):
            x_i = float(x_i_int) / INT_MULTIPLIER
            mult = 1.0
            for x_j_int in self.point_ranges(self.N):
                if x_j_int != x_i_int:
                    x_j = float(x_j_int) / INT_MULTIPLIER
                    mult *= (x - x_j) / (x_i - x_j)
            ans += self.calc(Solution.ci, x_i) * mult

        return ans

    def err(self, x):
        return abs(self.calc(Solution.ln, x) - self.calc(Solution.ci, x))


def tabulate(sol, functions):
    for x_int in Solution.range_regular(sol.M):
        x = x_int / INT_MULTIPLIER
        row = "{0:.2f} ".format(x)
        for f in functions:
            row += "{0:.6f} ".format(sol.calc(f, x))
        print(row)


def print_max(sol, f):
    max = f(Solution.range_regular(sol.M).start/INT_MULTIPLIER)

    for x_int in Solution.range_regular(sol.M):
        x = x_int / INT_MULTIPLIER
        ans = f(x)
        max = max if max>=ans else ans

    print("Maximum: {0:.6f}".format(max))


def print_max_with_n(sol, f, n):
    max = f(Solution.range_regular(sol.M).start/INT_MULTIPLIER)

    for x_int in Solution.range_regular(sol.M):
        x = x_int / INT_MULTIPLIER
        ans = f(x)
        max = max if max>=ans else ans

    print("{0:2d} {1:.8f}".format(n, max))


print("------------------- x f(x) ----------------------------")
simple_tabulating = [Solution.ci]
tabulate(Solution(5, 10, Solution.range_regular), simple_tabulating)

print("------------------- in check points -------------------")
sol = Solution(5, 5, Solution.range_regular)
print("ci({0:.2f}) = {1:.6f}".format(0.8, sol.ci(0.8)))
print("ci({0:.2f}) = {1:.6f}".format(2, sol.ci(2)))
print("ci({0:.2f}) = {1:.6f}".format(3.2, sol.ci(3.2)))

print("\n------------ x f(x) Ln(x) eps(x) ----------------------")
lagrange_tabulating = [Solution.ci, Solution.ln, Solution.err]
sol = Solution(5, 10, Solution.range_regular)
tabulate(Solution(5, 10, Solution.range_regular), lagrange_tabulating)
print_max(sol, sol.err)

print("\n------------ x f(x) Ln(x) eps(x) ----------------------")
sol = Solution(5, 10, Solution.range_cheb)
tabulate(sol, lagrange_tabulating)
print_max(sol, sol.err)

for i in range(7, 50, 2):
    sol = Solution(i, 10, Solution.range_regular)
    print_max_with_n(sol, sol.err, i)

for i in range(7, 50, 2):
    sol = Solution(i, 10, Solution.range_cheb)
    print_max_with_n(sol, sol.err, i)