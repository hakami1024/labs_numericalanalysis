from numpy import *

n = 40
h = 1/float(n)


def g(x):
    return 1 + x


def u(x):
    return x*(1-x)**3


def p(x):
    return 1 + x**2


def f(x):
    return 6-19*x+28*x**2-36*x**3+22*x**4 - x**5


def progonka(y, i, coef_yi, coef_f):
    a = -p(i*h)
    b = p(i*h) + p((i+1)*h) + h**2*g(h*i)
    c = -p((i+1)*h)

    next_coef_f = (f(h*i)*h**2 - a*coef_f)/(b + a*coef_yi)

    if i < n-1:
        next_coef_yi = -c/(b + a*coef_yi)
        progonka(y, i + 1, next_coef_yi, next_coef_f)
        y[i] = next_coef_yi * y[i+1] + next_coef_f
    else:
        y[i] = next_coef_f


def naiskoreishiy_spusk():
    y = zeros(n+1)
    matr = zeros((n+1)**2).reshape(n+1, n+1)
    for i in range(1, n):
        if i>1:
            matr[i][i-1] = -p(i*h)
        matr[i][i] = p(i*h) + p((i+1)*h) + h*h*g(h*i)
        if i<n-1:
            matr[i][i+1] = -p((i+1)*h)

    b = zeros(n+1)
    b[1:-1] = [ f(i*h)*h**2 for i in range(1, n)]

    steps = 0
    r = (dot(matr, y) - b)
    while max([abs(num) for num in r]) >= h**4:
        t = dot(r, r)/dot(dot(matr, r), r)
        last_y = y.copy()
        y = (last_y - t*(dot(matr, last_y) - b))
        r = (dot(matr, y) - b)
        steps += 1

    return y, steps


def zeidel():
    y = zeros(n+1)
    matr = zeros((n+1)*(n+1)).reshape(n+1, n+1)
    for i in range(1, n):
        if i>1:
            matr[i][i-1] = -p(i*h)
        matr[i][i] = p(i*h) + p((i+1)*h) + h**2*g(h*i)
        if i<n-1:
            matr[i][i+1] = -p((i+1)*h)

    c = zeros((n+1)*(n+1)).reshape(n+1, n+1)
    for i in range(1, n):
        c[i] = matr[i]/matr[i][i]
        c[i][i] = 0

    rez = zeros(n+1)
    rez[1:-1] = [ (f(i*h)*h**2)/matr[i][i] for i in range(1, n)]
    b = zeros(n+1)
    b[1:-1] = [f(i*h)*h**2 for i in range(1, n)]

    steps = 0
    r = max([abs(num) for num in (dot(matr, y) - b)])

    while r >= h**4:
        for i in range(1, n):
            y[i] = rez[i] - dot(c[i], y)
        steps+=1
        r = max([abs(num) for num in (dot(matr, y) - b)])

    return y, steps


def relaxation(omega):
    y = zeros(n+1)
    matr = zeros((n+1)*(n+1)).reshape(n+1, n+1)
    for i in range(1, n):
        if i>1:
            matr[i][i-1] = -p(i*h)
        matr[i][i] = p(i*h) + p((i+1)*h) + h*h*g(h*i)
        if i<n-1:
            matr[i][i+1] = -p((i+1)*h)

    c = zeros((n+1)*(n+1)).reshape(n+1, n+1)
    for i in range(1, n):
        c[i] = matr[i]/matr[i][i]
        c[i][i] = 0
    b = zeros(n+1)
    b[1:-1] = [f(i*h)*h**2 for i in range(1, n)]

    rez = zeros(n+1)
    rez[1:-1] = [ f(i*h)*h**2/matr[i][i] for i in range(1, n)]

    steps = 0
    r = max([abs(num) for num in (dot(matr, y) - b)])
    while r >= h**4:
        for i in range(1, n):
            y[i] = omega*(rez[i] - dot(c[i], y)) + (1 - omega)*y[i]
        steps+=1
        d = dot(matr, y) - b
        r = max([abs(num) for num in (dot(matr, y) - b)])

    return y, steps


y_presize = zeros(n+1)

progonka(y_presize, 1, 0, 0)
print("h^2 = {0:.6f}".format(h**2))

print("{4:2s} {0:5s} {1:12s} {2:12s} {3:12s}".format("i*h", "yi", "u(i*h)", "|yi - u(i*h)|", "i"))
for i in range(0, n+1):
    print("{4:2d} {0:.3f} {1:2.10f} {2:2.10f} {3:2.10f}".format(i*h, y_presize[i], u(i*h), abs(y_presize[i] - u(i*h)), i))

y_naisk, k = naiskoreishiy_spusk()
print("{4:2s} {0:5s} {1:12s} {2:12s} {3:12s}".format("i*h", "yi", "yi{k = "+str(k)+"}(i*h)", "|yi - yik(i*h)|", "i"))
for i in range(0, n+1):
    print("{4:2d} {0:.3f} {1:2.10f} {2:2.10f} {3:2.10f}".format(i*h, y_presize[i], y_naisk[i], abs(y_presize[i] - y_naisk[i]), i))

y_zeidel, k = zeidel()
print("{4:2s} {0:5s} {1:12s} {2:12s} {3:12s}".format("i*h", "yi", "yi{k = "+str(k)+"}(i*h)", "|yi - yik(i*h)|", "i"))
for i in range(0, n+1):
    print("{4:2d} {0:.3f} {1:2.10f} {2:2.10f} {3:2.10f}".format(i*h, y_presize[i], y_zeidel[i], abs(y_presize[i] - y_zeidel[i]), i))

min_omega = None
omega_val = Infinity
for i in range(1, 20):
    omega = 1.0 + i/20.0
    y_relax, k = relaxation(omega)
    if k < omega_val:
        omega_val = k
        min_omega = omega
    print("{0:.3f} {1:5d}".format(omega, k))

print("min_omega = " + str(min_omega))
y_relax, k = relaxation(min_omega)
print("{4:2s} {0:5s} {1:12s} {2:12s} {3:12s}".format("i*h", "yi", "yi{k = "+str(k)+"}(i*h)", "|yi - yik(i*h)|", "i"))
for i in range(0, n+1):
    print("{4:2d} {0:.3f} {1:2.10f} {2:2.10f} {3:2.10f}".format(i*h, y_presize[i], y_zeidel[i], abs(y_presize[i] - y_zeidel[i]), i))



