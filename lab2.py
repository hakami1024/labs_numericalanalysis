from math import *

__author__ = 'hakami'

INT_MULTIPLIER = 10 ** 7


def frange(a, b, h):
    while a < b:
        yield a
        a += h

class Solution:

    def __init__(self, func):
        self.__cashed_values = {}
        self.func = func
        self.count = 0

    def calc(self, func, x):
        x_int = int(x * INT_MULTIPLIER)
        if func in self.__cashed_values and x_int in self.__cashed_values[func]:
            return self.__cashed_values[func][x_int]
        else:
            answer = func(self, x)
            if func not in self.__cashed_values:
                self.__cashed_values[func] = {}
            self.__cashed_values[func][x_int] = answer
            return answer

    def ci(self, x):
        n = 1.0
        elem = -1 * x ** 2 / 2.0
        ans = 0.0
        while abs(elem / (2 * n)) > 10.0 ** -7:
            ans += elem / (2 * n)
            n += 1.0
            elem *= -1 * x ** 2
            elem /= 2 * n * (2 * n - 1)

        return ans

    def last_count(self, x):
        return self.count

    def sn(self, x):
        lastsum = 0
        sum = self.func(x, 2)
        n = 4

        while n < 2048 and abs(sum - lastsum) > 10**-6:
            lastsum = sum
            sum = self.func(x, n)
            n *= 2

        self.count = n/2
        return sum

    @staticmethod
    def f(t):
        if t == 0:
            return 0
        return (cos(t)-1.0)/t

    @staticmethod
    def left(x, n):
        h = x/float(n)
        result = 0

        for i in frange(0, x, h):
            if x-i < h*0.5:
                return result

            result += h*Solution.f(i)

        return result

    @staticmethod
    def center(x, n):
        h = x/float(n)
        result = 0

        for i in frange(0, x, h):
            if x-i < h*0.5:
                return result

            result += h*Solution.f(i+h*0.5)

        return result

    @staticmethod
    def trapeze(x, n):
        h = x/float(n)
        result = 0

        for i in frange(0, x, h):
            if x-i < h*0.5:
                return result

            result += h*0.5*(Solution.f(i)+Solution.f(i+h))

        return result

    @staticmethod
    def simpson(x, n):
        h = x/float(n)
        result = 0

        for i in frange(0, x, h):
            if x-i < h*0.5:
                return result

            result += h*(Solution.f(i) + 4*Solution.f(i+h*0.5) + Solution.f(i+h))/6.0

        return result

    @staticmethod
    def gauss(x, n):
        h = x/float(n)
        result = 0

        for i in frange(0, x, h):
            if x-i < h*0.5:
                return result

            result += h*0.5*(Solution.f(i+h*0.5*(1-1/sqrt(3))) + Solution.f(i+h*0.5*(1+1/sqrt(3))))

        return result

    def err(self, x):
        return abs(self.calc(Solution.sn, x) - self.calc(Solution.ci, x))


def tabulate(sol, functions):
    h = 0.36
    for x in frange(0.4, 4.1, h):
        row = "{0:.2f} ".format(x)
        for f in functions:
            ans = sol.calc(f, x)
            row += ("{0:.7f} " if not ans.is_integer() else "{0:4.0f} ").format(ans)
        print(row)

tabulating = [Solution.ci, Solution.sn, Solution.last_count, Solution.err]

print("\n-----------------Left rectangles method-------------------------")
print("  x   f(x)      Sn(x)      n    eps(x)")
sol = Solution(Solution.left)
tabulate(sol, tabulating)

print("\n-----------------Center rectangles method-------------------------")
print("  x   f(x)      Sn(x)      n    eps(x)")
sol = Solution(Solution.center)
tabulate(sol, tabulating)

print("\n-----------------Trapezion method----------------------")
print("  x   f(x)      Sn(x)      n    eps(x)")
sol = Solution(Solution.trapeze)
tabulate(sol, tabulating)

print("\n-----------------Simpson method-------------------------")
print("  x   f(x)      Sn(x)      n    eps(x)")
sol = Solution(Solution.simpson)
tabulate(sol, tabulating)

print("\n-----------------Gauss method-------------------------")
print("  x   f(x)      Sn(x)      n    eps(x)")
sol = Solution(Solution.gauss)
tabulate(sol, tabulating)